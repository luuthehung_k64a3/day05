<?php
@include_once 'SignUpController.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/css/bootstrap-responsive.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.js"></script>
    <style>
        .error {
            color: red;
            font-size: 12px;

        }

        .note {
            color: red;
        }
    </style>
</head>

<body>
    <div class="center">
        <div class="login">
            <div class="login_wrap">
                <div class="error"><?php echo isset($err['name']) ? $err['name'] : "" ?></div>
                <div class="error"><?php echo isset($err['gender']) ? $err['gender'] : "" ?></div>
                <div class="error"><?php echo isset($err['department']) ? $err['department'] : "" ?></div>
                <div class="error"><?php echo isset($err['birthday']) ? $err['birthday'] : "" ?></div>
                <div class="error"><?php echo isset($err['address']) ? $err['address'] : "" ?></div>
                <div class="error"><?php echo isset($err['image']) ? $err['image'] : "" ?></div>
                <form method="POST" enctype="multipart/form-data">
                    <?php echo "<div class='cssLogin'>
                        <label class='loginInput' >Họ và tên  <span class='note'>*</span> </label>
                        <input type='text' name='fullName'>
                    </div>";
                    ?>


                    <?php


                    $gender = array(0 => "Nam", 1 => "Nữ");
                    $arrlength = count($gender);
                    echo "<label class='gender'> Giới tính   <span class='note'>*</span></label>";
                    for ($x = 0; $x < $arrlength; $x++) {
                        echo "<input id='gender$x' type='radio' name='gender' value='{{$gender[$x]}}'>";
                        echo $x == 0 ? "Nam" : "Nữ";
                    }
                    ?>

                    <div class='selectValue'>
                        <label class='divideValues'> Phân Khoa <span class='note'>*</span></label>

                        <select name='department'>
                            <option value=''></option>"
                            <?php
                            $array = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

                            foreach ($array as $key => $value) {

                                echo "<option value='$key' > $value</option>";
                            }

                            ?>


                        </select>
                    </div>

                    <div class='selectValue d-flex'>
                        <label class='divideValue'> Ngày sinh <span class='note'>*</span></label>
                        <div class="input-group input-daterange">

                            <input type="text" id="end" class="form-control text-left ml-2" name='birthday' placeholder="dd/mm/yy">
                            <span class="fa-solid fa-calendar-days"></span>
                        </div>
                    </div>


                    <?php echo "<div class='cssLogin'>
                        <label class='loginInput'  >Địa chỉ </label>
                        <input type='text' name='address'>
                    </div>";
                    ?>
                    <div class='Image d-flex'>
                        <label class='Image' style="width:60%;background-color:#71ad47;height:35px; border:1.5px solid #1976d2; font-size: 14px; color:white; padding: 5px 10px 5px 15px; margin-right :10px">Hình ảnh</label>
                        <input style="font-size: 12px;height:30px" type='file' id="input-image-3" name='uploadfile' onchange='readURL(this);' />

                    </div>
                    <?php echo "<div class='push'><button class='submit' type='submit' name='submit'>Đăng ký</button></div>" ?>
                    <input class="img_encode" name="img_encode" style="display: none;" />

                </form>

            </div>


        </div>
    </div>
</body>

</html>
<script>
    $(document).ready(function() {

        $('.input-daterange').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            calendarWeeks: true,
            clearBtn: true,
            disableTouchKeyboard: true
        });

    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.img_encode').val(e.target.result);

            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#input-image-3").fileinput({
        allowedFileExtensions: ["jpg", "jpeg"],
    });
</script>